﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextAtCam : MonoBehaviour
{

    Camera cam;

    // Use this for initialization 
    void Start()
    {
        cam = Camera.main;

    }

    // Update is called once per frame 
    void LateUpdate()
    {
        transform.LookAt(cam.transform);
        transform.rotation = Quaternion.LookRotation(cam.transform.forward);
    }
}