﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampRiddle : MonoBehaviour
{
    public GameObject l1;
    public GameObject l2;
    public GameObject l3;
    public Transform KL;
    private bool done = false;
    

    private void Update()
    {
        if(l1.GetComponent<Lamp>().activated && l2.GetComponent<Lamp>().activated && l3.GetComponent<Lamp>().activated && !done && correctOrder())
        {
            KL.Translate(0, -3.5f, 0);
            done = true;
            l1.GetComponent<Lamp>().enabled = false;
            l2.GetComponent<Lamp>().enabled = false;
            l3.GetComponent<Lamp>().enabled = false;
            l3.GetComponent<Lamp>().TextA.enabled = false;
        }
    }

    private bool correctOrder()
    {
        if (l1.GetComponent<Lamp>().activationTime < l2.GetComponent<Lamp>().activationTime && l2.GetComponent<Lamp>().activationTime < l3.GetComponent<Lamp>().activationTime)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
