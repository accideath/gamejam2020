﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bodypart : MonoBehaviour
{
    public MeshRenderer Text;
    public bool triggered;
    public Collider player;

    private void OnTriggerEnter(Collider Player)
    {
        triggered = true;
    }
    private void OnTriggerExit(Collider Player)
    {
        triggered = false;
    }

    

    private void Update()
    {
        if (triggered && !player.GetComponent<FPController>().head && !player.GetComponent<FPController>().legL && !player.GetComponent<FPController>().legR && !player.GetComponent<FPController>().armR && !player.GetComponent<FPController>().armL)
        {
            Text.enabled = true;
        }
        else
        {
            Text.enabled = false;
        }
        if (Input.GetButtonDown("Jump") && !player.GetComponent<FPController>().head && !player.GetComponent<FPController>().legL && !player.GetComponent<FPController>().legR && !player.GetComponent<FPController>().armR && !player.GetComponent<FPController>().armL && triggered)
        {
            tag = this.tag;
            switch (tag)
            {
                case "head":
                    player.GetComponent<FPController>().head = true;
                    break;
                case "legl":
                    player.GetComponent<FPController>().legL = true;
                    break;
                case "legr":
                    player.GetComponent<FPController>().legR = true;
                    break;
                case "arml":
                    player.GetComponent<FPController>().armL = true;
                    break;
                case "armr":
                    player.GetComponent<FPController>().armR = true;
                    break;
            }
            Destroy(gameObject);
        }
    }
}
