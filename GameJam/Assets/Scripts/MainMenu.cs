﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public GameObject player;
    public GameObject mMenu;
    public GameObject pMenu;
    public GameObject oMenu;
    public GameObject armL;
    public GameObject armR;
    public GameObject legL;
    public GameObject legR;
    public GameObject head;

    private bool mainPause;

    private void Start()
    {
        Time.timeScale = 1;
        


    }

    private void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            if(Time.timeScale == 1)
            {
                Time.timeScale = 0;
                //pMenu.SetActive(true);
                back();
                Cursor.lockState = CursorLockMode.None;
                player.GetComponent<FPController>().enabled = false;
                player.GetComponentInChildren<MouseCamLook>().enabled = false;
            }
            else
            {
                pauseButton();
            }
        }

        if (player.GetComponent<FPController>().armL && !pMenu.activeSelf && !oMenu.activeSelf)
        {
            armL.SetActive(true);
        }
        else if (armL.activeSelf)
        {
            armL.SetActive(false);
        }
        if (player.GetComponent<FPController>().armR && !pMenu.activeSelf && !oMenu.activeSelf)
        {
             armR.SetActive(true);
        }
        else if (armR.activeSelf)
        {
            armR.SetActive(false);
        }
        if (player.GetComponent<FPController>().legL && !pMenu.activeSelf && !oMenu.activeSelf)
        {
            legL.SetActive(true);
        }
        else if (legL.activeSelf)
        {
            legL.SetActive(false);
        }
        if (player.GetComponent<FPController>().legR && !pMenu.activeSelf && !oMenu.activeSelf)
        {
            legR.SetActive(true);
        }
        else if (legR.activeSelf)
        {
            legR.SetActive(false);
        }
        if (player.GetComponent<FPController>().head && !pMenu.activeSelf && !oMenu.activeSelf)
        {
            head.SetActive(true);
        }
        else if (head.activeSelf)
        {
            head.SetActive(false);
        }
        
       




    }
    public void PlayGame()
    {
        player.GetComponent<FPController>().enabled = true;
        player.GetComponentInChildren<MouseCamLook>().enabled = true;
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    
    public void main()
    {
        mainPause = true;
    }
    public void pause()
    {
        mainPause = false;
    }

    public void pauseButton()
    {
        Time.timeScale = 1;
        pMenu.SetActive(false); 
        oMenu.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        player.GetComponent<FPController>().enabled = true;
        player.GetComponentInChildren<MouseCamLook>().enabled = true;
    }

    public void back()
    {
        if (mainPause)
        {
            mMenu.SetActive(true);
            oMenu.SetActive(false);
            pMenu.SetActive(false);//SPÄTER MINECRAFT??? Ja heute Abend. nice alla. xD 
        }
        else
        {
            pMenu.SetActive(true);
            oMenu.SetActive(false);
            mMenu.SetActive(false);
        }
    }

}
