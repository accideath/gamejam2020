﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockPickingMovement : MonoBehaviour
{
    private Quaternion target_Rotation = Quaternion.identity;

    // Update is called once per frame
    void LateUpdate()
    {
        if (!(Input.GetAxis("Submit") > 0.5f))
        {
            if (Input.GetAxis("Vertical") > 0.3f)
            {
                target_Rotation = Quaternion.AngleAxis(180, transform.right);
            }
            else if (Input.GetAxis("Vertical") < -0.3f)
            {
                target_Rotation = Quaternion.AngleAxis(0, transform.right);
            }
            if (Input.GetAxis("Horizontal") > 0.3f)
            {
                target_Rotation = Quaternion.AngleAxis(270, transform.right);
            }
            else if (Input.GetAxis("Horizontal") < -0.3f)
            {
                target_Rotation = Quaternion.AngleAxis(90, transform.right);
            }
            transform.rotation = Quaternion.Slerp(transform.rotation, target_Rotation, 10 * Time.deltaTime);
        }
    }
}
