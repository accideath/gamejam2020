﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickingtool_Movement : MonoBehaviour
{
    private bool shaking = false;
    private float shaketimer = 0.2f;

    private Vector3 initPos;
    private Quaternion initRotation;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Submit")> 0.05f && !shaking)
        {
            shaking = true;
            initPos = transform.position;
            initRotation = transform.rotation;
        }
        if (shaking)
        {
            if(shaketimer >= 0)
            {
                Shake();
                shaketimer -= Time.deltaTime;
            }
            else
            {
                shaketimer = 0.05f;
                shaking = false;
                transform.position = initPos;
                transform.rotation = initRotation;
            }
        }
    }
    private void Shake()
    {
        transform.position = initPos + Random.insideUnitSphere * 0.008f;
    }
}
