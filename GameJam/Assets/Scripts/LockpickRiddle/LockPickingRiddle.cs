﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;


public class LockPickingRiddle : MonoBehaviour
{

    public TextMeshPro revealtext;
    public string playerName;
    public int maxLength;
    public int[] sequence;
    private int iterator;
    private int currentRiddle;

    private float antidoubleInputTimer = 0.25f;
    // Start is called before the first frame update
    void Start()
    {
        InitSequence();
        iterator = 0;
        currentRiddle = sequence[iterator];
    }

    // Update is called once per frame
    void Update()
    {
        //reduce input timer
        if(antidoubleInputTimer > 0)
        {
            antidoubleInputTimer -= Time.deltaTime;
        }
        if(revealtext.color.a > 0)
        {
            Color color = revealtext.color;
            color.a -= Time.deltaTime / 5;
            revealtext.color = color;
        }
        if (Input.GetAxis("Submit") > 0.5f && antidoubleInputTimer <= 0)
        {
            if (CheckIterator().Equals(currentRiddle))
            {
                antidoubleInputTimer = 0.25f;
                Debug.Log("riddle partly solved");
                iterator++;
                if (iterator >= sequence.Length)
                {
                    Debug.Log("riddle solved");
                    //riddle solved
                    //TODO RETURN TO PREVIOUS SCENE
                    //TODO change gamestate
                }
                else
                {
                    currentRiddle = sequence[iterator];
                }
            }
        }
    }
    private void InitSequence()
    {
        if (playerName.Length <= maxLength)
        {
            sequence = new int[playerName.Length];
            for (int i = 0; i < playerName.Length; i++)
            {
                sequence[i] = Mathf.Abs((playerName[i].GetHashCode() * 3 * i) % 4);
            }
        }
        else
        {
            sequence = new int[maxLength];
            for (int i = 0; i < maxLength; i++)
            {
                sequence[i] = Mathf.Abs((playerName[i].GetHashCode() * 3 * i) % 4);
            }
        }
        StringBuilder outputText = new StringBuilder();
        foreach(int i in sequence)
        {
            switch (i)
            {
                case 0:
                    outputText.Append(" ↑ ");
                    break;
                case 1:
                    outputText.Append("→");
                    break;
                case 2:
                    outputText.Append(" ↓ ");
                    break;
                case 3:
                    outputText.Append("←");
                    break;
                default:
                    outputText.Append("←");
                    break;
            }
        }
        revealtext.text = outputText.ToString();
    }

    int CheckIterator()
    {
            if (Input.GetAxis("Vertical") > 0.6f)
            {
                return 0;
            }
            else if (Input.GetAxis("Vertical") < -0.6f)
            {
                return 2;
            }
            else if (Input.GetAxis("Horizontal") > 0.6f)
            {
                return 1;
            }
            else
            {
                return 3;
            }
    }
}
