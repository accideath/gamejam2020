﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpen : MonoBehaviour
{
    public GameObject door;
    public GameObject doorR;
    private bool triggered;
    public MeshRenderer TextF;
    public MeshRenderer TextB;
    public MeshRenderer TextC;

    private void Update()
    {
        if (Input.GetButtonDown("Jump") && triggered)
        {
            if (!door.GetComponent<Door>().open)
            {
                door.GetComponent<Transform>().Rotate(0, -90, 0);
                if (doorR)
                {
                    doorR.GetComponent<Transform>().Rotate(0, 90, 0);
                }
                door.GetComponent<Door>().open = true;
            }
            else
            {
                door.GetComponent<Transform>().Rotate(0, 90, 0);
                if (doorR)
                {
                    doorR.GetComponent<Transform>().Rotate(0, -90, 0);

                }
                door.GetComponent<Door>().open = false;
            }
        }
        if (triggered && !door.GetComponent<Door>().open)
        {
            TextF.enabled = true;
            TextB.enabled = true;
            TextC.enabled = false;
        }
        else if (triggered && door.GetComponent<Door>().open)
        {
            TextF.enabled = false;
            TextB.enabled = false;
            TextC.enabled = true;
        }
        else
        {
            TextF.enabled = false;
            TextB.enabled = false;
            TextC.enabled = false;
        }

    }
    private void OnTriggerEnter(Collider Player)
    {
        triggered = true;
    }
    private void OnTriggerExit(Collider Player)
    {
        triggered = false;
    }
}
