﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class corpse : MonoBehaviour
{
    public bool triggered;
    public GameObject player;
    public MeshRenderer Text;
    public GameObject armL;
    public GameObject armR;
    public GameObject legL;
    public GameObject legR;
    public GameObject head;

    private void OnTriggerEnter(Collider Player)
    {
        triggered = true;
    }
    private void OnTriggerExit(Collider Player)
    {
        triggered = false;
    }

    private void Update()
    {
        if (triggered)
        {
            if (Input.GetButtonDown("Jump"))
            {
                if (player.GetComponent<FPController>().head)
                {
                    player.GetComponent<FPController>().head = false;
                    head.SetActive(true);
                }
                if (player.GetComponent<FPController>().armL)
                {
                    player.GetComponent<FPController>().armL = false;
                    armL.SetActive(true);
                }
                if (player.GetComponent<FPController>().armR)
                {
                    player.GetComponent<FPController>().armR = false;
                    armR.SetActive(true);
                }
                if (player.GetComponent<FPController>().legL)
                {
                    player.GetComponent<FPController>().legL = false;
                    legL.SetActive(true);
                }
                if (player.GetComponent<FPController>().legR)
                {
                    player.GetComponent<FPController>().legR = false;
                    legR.SetActive(true);
                }
            }
            
            if(player.GetComponent<FPController>().head || player.GetComponent<FPController>().armL || player.GetComponent<FPController>().armR || player.GetComponent<FPController>().legL || player.GetComponent<FPController>().legR)
            {
                Text.enabled = true;
            }
            else
            {
                Text.enabled = false;
            }



        }
        else
        {
            Text.enabled = false;
        }
    }
}
