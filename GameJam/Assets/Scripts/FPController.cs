﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPController : MonoBehaviour
{
    public float speed = 10f;
    public float runMultiplier = 1.5f;
    private float currentspeed;
    private float vertical;
    private float horizontal;

    public bool head;
    public bool armR;
    public bool armL;
    public bool legL;
    public bool legR;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Fire3")>0)
        {
            currentspeed = speed * runMultiplier;
        }
        else
        {
            currentspeed = speed;
        }
        vertical = Input.GetAxis("Vertical") * currentspeed * Time.deltaTime;
        horizontal = Input.GetAxis("Horizontal") * currentspeed * Time.deltaTime;
        transform.Translate(horizontal, 0, vertical);
        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
