﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Lamp : MonoBehaviour
{
    public bool activated;
    private bool triggered;
    public MeshRenderer TextA;
    public MeshRenderer TextD;
    public float activationTime;
    

    private void Update()
    {
        if (triggered && !activated)
        {
            if(Input.GetButtonDown("Jump"))
            {
                this.GetComponent<Transform>().Rotate(0, 0, -20);

                activated = true;
                activationTime = Time.time;
            }
            TextA.enabled = true;
            TextD.enabled = false;
        }
        else if(triggered && activated)
        {
            if (Input.GetButtonDown("Jump"))
            {
                this.GetComponent<Transform>().Rotate(0, 0, 20);

                activated = false;
            }
            TextD.enabled = true;
            TextA.enabled = false;
        }
        else
        {
            TextA.enabled = false;
            TextD.enabled = false;
        }

    }
    private void OnTriggerEnter(Collider Player)
    {
        triggered = true;
    }
    private void OnTriggerExit(Collider Player)
    {
        triggered = false;
    }
}
